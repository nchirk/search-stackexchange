package com.example.searchstackexchange.consts;

public enum SortField {
    activity,
    creation,
    votes,
    relevance
}
