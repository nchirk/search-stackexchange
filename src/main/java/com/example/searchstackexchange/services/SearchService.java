package com.example.searchstackexchange.services;

import com.example.searchstackexchange.services.models.SearchParams;
import com.example.searchstackexchange.services.models.SearchResults;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Service
public class SearchService {
    //Описание метода https://api.stackexchange.com/docs/search
    private final String serviceUrl = "https://api.stackexchange.com/2.2/search?page={page}&pagesize={pagesize}&order={order}&sort={sort}&intitle={intitle}&site={site}";
    private final RestTemplate restTemplate;

    //TODO с более развитой фронтовой частью можно сделать эти параметры изменямыми
    public final int pageSize = 20;
    public final String site = "stackoverflow";

    public SearchService(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    public SearchResults search(SearchParams searchParams){
        var searchString = searchParams.getSearchString();
        var page = searchParams.getPage();
        var order = searchParams.getSortOrder();
        var sort = searchParams.getSort();

        if (searchString == null || searchString.isBlank() || page < 1){
            return new SearchResults();
        }

        var requestParams = new HashMap<String, String>()
        {
            {
                put("intitle", searchString);
                put("page", String.valueOf(page));
                put("pagesize", String.valueOf(pageSize));
                put("order", order.name());
                put("sort", sort.name());
                put("site", site);
            }
        };

        var result = restTemplate.getForObject(serviceUrl, SearchResults.class, requestParams);
        return result;
    }
}
