package com.example.searchstackexchange.web.models;

import com.example.searchstackexchange.consts.SortField;
import com.example.searchstackexchange.consts.SortOrder;

import java.io.Serializable;

public class SearchParamsDTO implements Serializable {
    private String searchString;
    private int page = 1;
    private SortField sort;
    private SortOrder sortOrder;

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public SortField getSort() {
        return sort;
    }

    public void setSort(SortField sort) {
        this.sort = sort;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
