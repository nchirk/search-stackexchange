package com.example.searchstackexchange.services.models;

import com.example.searchstackexchange.consts.SortField;
import com.example.searchstackexchange.consts.SortOrder;

public class SearchParams {
    private String searchString;
    private int page;
    private SortField sort;
    private SortOrder sortOrder;

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public SortField getSort() {
        return sort;
    }

    public void setSort(SortField sort) {
        this.sort = sort;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
