package com.example.searchstackexchange.web;

import com.example.searchstackexchange.services.SearchService;
import com.example.searchstackexchange.services.models.SearchParams;
import com.example.searchstackexchange.web.models.SearchParamsDTO;
import com.example.searchstackexchange.web.models.SearchResultsDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SearchController {

    private final SearchService searchService;
    private final ModelMapper modelMapper;

    public SearchController(SearchService searchService, ModelMapper modelMapper) {
        this.searchService = searchService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping(value={"", "/", "/search"}, method=RequestMethod.GET)
    public String get(Model model) {
        model.addAttribute("searchResults", new SearchResultsDTO());
        model.addAttribute("searchParams", new SearchParamsDTO());
        return "search";
    }

    @RequestMapping(value="/search", method=RequestMethod.POST, params="action=newSearch")
    public String newSearch(Model model, @ModelAttribute SearchParamsDTO searchParams) {
        searchParams.setPage(1);
        return GenerateSearchView(model, searchParams);
    }

    @RequestMapping(value="/search", method=RequestMethod.POST, params="action=nextPage")
    public String nextPage(Model model, @ModelAttribute SearchParamsDTO searchParams) {
        var currentPage = searchParams.getPage();
        searchParams.setPage(currentPage + 1);
        return GenerateSearchView(model, searchParams);
    }

    @RequestMapping(value="/search", method=RequestMethod.POST, params="action=prevPage")
    public String prevPage(Model model, @ModelAttribute SearchParamsDTO searchParams) {
        var page = searchParams.getPage();
        if (page > 1) {
            page = page - 1;
        }
        searchParams.setPage(page);
        return GenerateSearchView(model, searchParams);
    }

    private String GenerateSearchView(Model model, SearchParamsDTO searchParamsDTO) {
        var searchParams = modelMapper.map(searchParamsDTO, SearchParams.class);
        var searchResults = searchService.search(searchParams);
        var searchResultsDTO = modelMapper.map(searchResults, SearchResultsDTO.class);
        model.addAttribute("searchParams", searchParamsDTO);
        model.addAttribute("searchResults", searchResultsDTO);

        return "search";
    }
}
