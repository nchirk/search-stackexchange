package com.example.searchstackexchange.web.models;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class ItemDTO {
    private boolean isAnswered;
    private LocalDateTime creationDate;
    private String link;
    private String title;
    private String author;
    private String authorLink;

    public boolean getIsAnswered() {
        return isAnswered;
    }

    public void setIsAnswered(boolean isAnswered) {
        this.isAnswered = isAnswered;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }
    public void setCreationDateFromUnixMilliseconds(long creationDateMilliseconds) {
        var instant = Instant.ofEpochSecond(creationDateMilliseconds);
        var timezone = TimeZone.getDefault().toZoneId();
        this.creationDate = LocalDateTime.ofInstant(instant, timezone);
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorLink() {
        return authorLink;
    }

    public void setAuthorLink(String authorLink) {
        this.authorLink = authorLink;
    }
}
