package com.example.searchstackexchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchStackexchangeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SearchStackexchangeApplication.class, args);
    }

}
