package com.example.searchstackexchange.services.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
    public class Item {

        @JsonProperty("owner")
        private Owner owner;
        @JsonProperty("is_answered")
        private boolean isAnswered;
        @JsonProperty("creation_date")
        private long creationDate;
        @JsonProperty("question_id")
        private int questionId;
        @JsonProperty("link")
        private String link;
        @JsonProperty("title")
        private String title;

        @JsonProperty("owner")
        public Owner getOwner() {
            return owner;
        }

        @JsonProperty("owner")
        public void setOwner(Owner owner) {
            this.owner = owner;
        }

        @JsonProperty("is_answered")
        public boolean getIsAnswered() {
            return isAnswered;
        }

        @JsonProperty("is_answered")
        public void setIsAnswered(boolean isAnswered) {
            this.isAnswered = isAnswered;
        }

        @JsonProperty("creation_date")
        public long getCreationDate() {
            return creationDate;
        }

        @JsonProperty("creation_date")
        public void setCreationDate(long creationDate) {
            this.creationDate = creationDate;
        }

        @JsonProperty("question_id")
        public int getQuestionId() {
            return questionId;
        }

        @JsonProperty("question_id")
        public void setQuestionId(int questionId) {
            this.questionId = questionId;
        }

        @JsonProperty("link")
        public String getLink() {
            return link;
        }

        @JsonProperty("link")
        public void setLink(String link) {
            this.link = link;
        }

        @JsonProperty("title")
        public String getTitle() {
            return title;
        }

        @JsonProperty("title")
        public void setTitle(String title) {
            this.title = title;
        }

    }

