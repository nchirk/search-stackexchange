package com.example.searchstackexchange.consts;

public enum SortOrder {
    desc, asc
}
