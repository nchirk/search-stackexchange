# Запуск
Предполагаю, что стандартный для Maven, я использовала средства IDE и команду `mvnw spring-boot:run`

# Комментарии по реализации
Стек технологий:
* Spring Boot MVC;
* Thymeleaf;
* RestTemplate.

Для веба выбрала Spring Boot MVC и Thymeleaf: приложение простое, также хотела узнать, как устроена шаблонизация в Java. Гуглеж показал, что эти технологии самые популярные.
В промышленной разработке мне привычнее делать сервисы для фронтовых приложений, при этом я выступаю как бэкендер, а не фулстек.

Для запросов в API выбрала RestTemplate как часть Spring. При более объемном использовании API я бы поискала хорошее SDK.
О выборе немного пожалела. RestTemplate оказался строго синхронным (что для данной задачи не критично, но в целом грустно), не мог расшифровать gzip-ответы и декодировать html entity в строках (а вот это уже совсем грустно!).
Хотя возможно я просто не умею его готовить :)
В итоге пробросила в RestTemplate http-клиент от apache. Подозреваю, что можно было сразу пользоваться этим клиентом и либой для json.

Помимо того, что явно требовалось, добавила пагинацию, и пару поисковых параметров.
Не сделала тесты и не стала добавлять более умную обработку enum-ов, не посчитала нужным. Также не посчитала нужным делать nullable-свойства в объектах для json-ов: даже если там придет null, по бизнес-логике они никак не используются, дефолтное значение типа подойдет.

# Комментарии по заданию
В задании есть следующая формулировка.

> Treat this project as you would any professional task - feel free to ask questions, clarify requirements,
and talk about your design and thinking with the team<...>

Скорее всего похожее задание используется для кодинга вживую, и это предложение актуально для него. На всякий случай напишу свои соображения, и вопросы, которые у меня бы возникли.  

Вопросы с точки зрения аналитики.
* Нужны ли в интерфейсе другие параметры кроме поисковой строки?
* Нужна ли пагинация, как она должна быть реализована: набором кнопок или автоматической подгрузкой, нужно ли задавать в интерфейсе размер страницы с результатами?
* Нужна ли возможность добавлять и убирать столбцы, настраивать отображение таблицы?
* В задании идет речь о пользователе: нужно ли подключать авторизацию в API?

Вопросы с точки зрения реализации.
* Есть ли предпочтения по стеку технологий помимо того, что нужно использовать Java?
* Как реализовать фронтенд и бэкенд: классический MVC c шаблонизацией или REST-сервис и отдельное фронтовое приложение?
* Нужна ли динамическая подгрузка данных или страницы должны перестраиваться при перезагрузке?
* Насколько стабильное и быстрое интернет-соединение, требуется ли кэширование?