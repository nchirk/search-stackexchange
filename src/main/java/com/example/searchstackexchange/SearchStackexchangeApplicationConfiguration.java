package com.example.searchstackexchange;

import com.example.searchstackexchange.services.models.Item;
import com.example.searchstackexchange.web.models.ItemDTO;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.HtmlUtils;

@Configuration
public class SearchStackexchangeApplicationConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.typeMap(Item.class, ItemDTO.class).addMappings(mapper -> {
            mapper.map(src -> src.getOwner().getDisplayName(), ItemDTO::setAuthor);
            mapper.map(src -> src.getOwner().getLink(), ItemDTO::setAuthorLink);
            mapper.map(Item::getCreationDate, ItemDTO::setCreationDateFromUnixMilliseconds);
        });

        modelMapper.addConverter(new Converter<String, String>() {
            @Override
            public String convert(MappingContext<String, String> context) {
                String source = context.getSource();
                if (source == null) {
                    return null;
                }
                return HtmlUtils.htmlUnescape(source);
            }
        });

        return modelMapper;
    }

    @Bean
    public CloseableHttpClient httpClient() {
        //Добавила http клиент от apache, потому что RestTemplate с параметрами по умолчанию не может прочитать gzip ответы
        var httpClient = HttpClientBuilder.create().build();
        return httpClient;
    }

    @Bean
    public RestTemplate restTemplate(HttpClient httpClient) {
        var requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        return new RestTemplate(requestFactory);
    }

}

